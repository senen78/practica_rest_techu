var moca = require('mocha');
var chai = require('chai');
var chaihttp = require('chai-http');

chai.use(chaihttp);

var should = chai.should();

var server = require('../server');

describe('First test',
	function(){
		it('Test that DuckDuckGo works',
			function(done){
				chai.request ('http://www.duckduckgo.com')
					.get('/')
					.end(
						function (err, res) {
							console.log("Request has ended");
							if (err != null) {
								console.log(err);
								console.log(res.body);
							}
							res.should.have.status(200);
							done();
						}
					)
				
			}
		)
	}
);

describe('Test de API de usuarios Tech U',
	function(){
		it('Prueba que la API de usuarios funciona correctamente',
			function(done){
				chai.request ('http://localhost:3000')
					.get('/apitechu/v1')
					.end(
						function (err, res) {
							console.log("Request has ended");
							if (err != null) {
								console.log(err);
								console.log(res.body);
							}
							res.should.have.status(200);
							res.body.msg.should.be.eql("Hola desde APITechU");
							done();
						}
					)
				
			}
		),
		
		it('Prueba que la API de usuarios funciona correctamente',
			function(done){
				chai.request ('http://localhost:3000')
					.get('/apitechu/v1/users')
					.end(
						function (err, res) {
							console.log("Request has ended");
							if (err != null) {
								console.log(err);
								console.log(res.body);
							}
							res.should.have.status(200);
							res.body.should.be.instanceof(Array).and.have.lengthOf(10);

							for (user of res.body)	{
								user.should.have.property('email');
								user.should.have.property('password');
							}						
							//res.body.should.be.instanceof(Array).with.property('first-name', 'Senén')
							done();
						}
					)
				
			}
		)
	}
);
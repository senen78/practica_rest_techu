const express = require('express');
const fs = require('fs');
const bodyParser = require('body-parser');
const requestJson = require('request-json');

const app = express();
const port = process.env.PORT || 3000;

const baseMLabURL = 'https://api.mlab.com/api/1/databases/apitechu/collections/';
const mLabAPIKey = 'apiKey=piLW6FpitsgH2jegVyia3sEDdb532Tvp';



app.use(bodyParser.json());
app.listen(port, () => console.log('App escuchando en el puerto ' + port));


app.get('/', (req, res) => res.send('Server UP!! Utiliza el API /apitechu'));

app.get('/status', (req, res) => res.send('App UP!!'));

app.get('/apitechu/v1',
	function(req, res){
		console.log("GET: /apitechu/v1");
		res.send({"msg" : "Hola desde APITechU"});
	}
);

app.get('/apitechu/v1/users',
	function(req, res){
		console.log("GET: /apitechu/v1/users");

		res.sendFile('users.json', {root: __dirname});
	}
);

app.post('/apitechu/v1/users',
	function(req, res){
		console.log("POST: /apitechu/v1/users");

		var newUser = {
			"first_name" : req.body.first_name,
			"second_name" : req.body.second_name,
			"email" : req.body.email,
			"country" : req.body.country
		}

		var users = require('./users.json');
		users.push(newUser);

		writeUserDataToFile(users)

		console.log("Usuario guardado con éxito");
		res.send({"msg":"Usuario guardado con éxito"});

	}
);

app.delete('/apitechu/v1/users/:id',
	function(req, res){
		console.log("DELETE: /apitechu/v1/users/:id");
		console.log(req.params.id);

		var users = require('./users2.json');
		users.pop();

		writeUserDataToFile(users)

		console.log("Usuario borrado con éxito");
		res.send({"msg":"Usuario borrado con éxito"});

	}
);



app.post('/apitechu/v1/params/:p1/:p2',
	function(req, res){
		console.log("POST: /apitechu/v1/monstruo/:p1/:p2");
		console.log(req.params.p1);
		console.log(req.params.p2);
		console.log(req.headers.first_name);
		console.log(req.body);
		console.log(req.query.queryparam1)
	}
);



app.post('/apitechu/v1/login',
	function(req, res){
		console.log("POST: /apitechu/v1/login");

		var username = req.body.email
		var password = req.body.password

		var users = require('./users.json');
		var msg = {"mensaje" : "Login incorrecto"};

		for (user of users) {
			if (user.email==username && user.password==password) {
				user.logged = true;
				writeUserDataToFile(users)
				msg = {
 							"mensaje" : "Login correcto",
 							"idUsuario" : user.id
							}
				break;
			}
		}
		res.send(msg);
	}
);


app.post('/apitechu/v1/logout',
	function(req, res){
		console.log("POST: /apitechu/v1/logout");

		var iduser = req.body.id

		var users = require('./users.json');
		var msg = {"mensaje" : "Logout incorrecto"};

		for (user of users) {
			if (user.id==iduser && user.logged) {
				delete user.logged;
				writeUserDataToFile(users);
				msg = {
 							"mensaje" : "Logout correcto",
 							"idUsuario" : user.id
							}
				break;
			}
		}

		res.send(msg);
	}
);




app.get('/apitechu/v2/users',
	function(req, res){
		console.log("GET: /apitechu/v2/users");
		var httpClient = requestJson.createClient(baseMLabURL);
		console.log("HTTP client created");

		httpClient.get("users?" + mLabAPIKey, 
						function(err, resMLab, body){
						  var response = !err ? body : {
						  	"msg" : "Error obteniendo usuarios"
						  };
						res.send(response);
						}
					);			
	}
);



app.get('/apitechu/v2/users/:id',
	function(req, res){
		console.log("GET: /apitechu/v2/users/:id");
		var id = req.params.id;
		var query = 'q={"id":' + id + '}';

		var httpClient = requestJson.createClient(baseMLabURL);
		console.log("HTTP client created");

		httpClient.get("users?" + query + "&" + mLabAPIKey, 
						function(err, resMLab, body){	
							 var response = {};
							 if (err) {						
							 	response = {
							  		"msg" : "Error obteniendo el usuario"
							 	};
								 res.status(500);

							 }  else {
							 	if (body.length > 0) {
							 		response = body;
							 	} else {				
								 	response = {
								  		"msg" : "Usuario no encontrado"
								 	};
								 	res.status(404);
							 	}
							 }
							 res.send(response);
						}
					);
	}
);

app.get('/apitechu/v2/users/:id/accounts',
	function(req, res){
		console.log("GET: /apitechu/v1/users/:id/accounts");
		var id = req.params.id;
		var query = 'q={"USERID":' + id + '}';

		var httpClient = requestJson.createClient(baseMLabURL);
		console.log("HTTP client created");

		var URI = "accounts?" + query + "&" + mLabAPIKey

		console.log(URI);

		httpClient.get(URI, 
						function(err, resMLab, body){	
							 var response = {};
							 if (err) {						
							 	response = {
							  		"msg" : "Error obteniendo el listado de cuentas del usuario"
							 	};
								 res.status(500);

							 }  else {
							 	if (body.length > 0) {
							 		response = body;
							 	} else {				
								 	response = {
								  		"msg" : "Usuario no encontrado o sin cuentas asociadas"
								 	};
								 	res.status(404);
							 	}
							 }
							 res.send(response);
						}
					);
	}
);
app.post('/apitechu/v2/login',
	function(req, res){
		console.log("POST: /apitechu/v2/login");

		var email = req.body.email
		var password = req.body.password
		
		var query = 'q={"email":"' + email + '","password":"' + password + '"}';

		var httpClient = requestJson.createClient(baseMLabURL);
		console.log("HTTP client created");

		httpClient.get("users?" + query + "&" + mLabAPIKey, 
						function(err, resMLab, body){
							var response ="";	
							if (err) {					
								response = {
									"msg" : "Error buscando usuario en login"
								};
								res.status(500);
								res.send(response);
							}  else {
								 	if (body.length > 0) {
								 		var idUser = body[0].id;
									 	var putBody = '{"$set":{"logged":true}}';
										httpClient.put("users?" + query + "&" + mLabAPIKey, JSON.parse(putBody),
											function(err, resMLab, body){
												 if (err) {						
												 	response = {
												  		"msg" : "Error actualizando el usuario"
												 	};
													res.status(500);
												 }  else {								 						
													 	response = {
													  		"msg" : "Usuario logado",
													  		"idUser" : idUser
													 	};
												 }
												res.send(response);
											}
										);
								 	} else {												 						
										response = {
											"msg" : "Usuario no encontrado"
										};
										res.status(404);
										res.send(response);
									};
							};
						}
		);
	}
);

app.post('/apitechu/v2/logout',
	function(req, res){
		console.log("POST: /apitechu/v2/logout");
		
		var iduser = req.body.id
		
		var query = 'q={"id":' + iduser + ', "logged":true}';

		var httpClient = requestJson.createClient(baseMLabURL);
		console.log("HTTP client created");

		httpClient.get("users?" + query + "&" + mLabAPIKey, 
						function(err, resMLab, body){
							var response ="";	
							if (err) {					
								response = {
									"msg" : "Error buscando usuario en logout"
								};
								res.status(500);
								res.send(response);
							}  else {
								 	if (body.length > 0) {
									 	var putBody = '{"$unset":{"logged":""}}';
										httpClient.put("users?" + query + "&" + mLabAPIKey, JSON.parse(putBody),
											function(err, resMLab, body){
												 if (err) {						
												 	response = {
												  		"msg" : "Error actualizando el usuario"
												 	};
													res.status(500);
												 }  else {								 						
													 	response = {
													  		"msg" : "Usuario deslogado"
													 	};
												 }
												res.send(response);
											}
										);
								 	} else {												 						
										response = {
											"msg" : "Usuario no logado"
										};
										res.status(404);
										res.send(response);
									};
							};
						}
		);
	}
);



function writeUserDataToFile(data){
	jsonUserData = JSON.stringify(data, undefined, 2);

	fs.writeFile('users.json', jsonUserData, "utf8",
		function (err) {
		  if (err) {
		  	var msg = "Error al escribir el usuario"
		  	console.log(msg);
		  } else {
		  	var msg = "Usuario grabado"
		  	console.log(msg);
		  }
	});	
}
